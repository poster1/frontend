#!/bin/bash

echo "cleaning up..."
rm -rf var/cache var/log vendor composer.lock .php_cs.cache .phpcs-cache node_modules package-lock.json
