import { createApp, markRaw } from "vue";
import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";

import App from "@/App.vue";
import i18n from "@/translations";
import router from "@/router";
import capitalizePlugin from "@/plugins/capitalizePlugin";

import "@/assets/main.scss";

const app = createApp(App);

const pinia = createPinia();
pinia.use(({ store }) => {
  store.router = markRaw(router);
});
pinia.use(piniaPluginPersistedstate);

app.use(pinia);
app.use(router);
app.use(i18n);
app.use(capitalizePlugin);

app.mount("#app");
