import { createI18n } from "vue-i18n";
import en from "@/translations/en";
import fr from "@/translations/fr";

export default createI18n({
  locale: "fr",
  fallbackLocale: "en",
  messages: {
    fr,
    en,
  },
});
