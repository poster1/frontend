export default {
  please_sign_in: "veuillez vous connecter",
  email: "email",
  password: "mot de passe",
  remember_me: "se souvenir de moi",
  forgot_your_password: "mot de passe oublié",
  sign_in: "se connecter",
  please_sign_up: "veuillez vous inscrire",
  first_name: "prénom",
  last_name: "nom",
  sign_up: "s'inscrire",
  or: "ou",
};
