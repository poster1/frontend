export default {
  please_sign_in: "please sign in",
  email: "email",
  password: "password",
  remember_me: "remember me",
  forgot_your_password: "forgot your password",
  sign_in: "sign in",
  please_sign_up: "please sign up",
  first_name: "first name",
  last_name: "last name",
  sign_up: "sign up",
  or: "or",
};
