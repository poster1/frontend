import type Post from "@/entities/Post";
import type User from "@/entities/User";
import type Rating from "@/entities/Rating";

export default class Comment {
  public id?: number | null = null;
  public content?: string | null = null;
  public post?: Post | null = null;
  public author?: User | null = null;
  public parent?: Comment | null = null;
  public children?: Comment[] | null = null;
  public ratings?: Rating[] | null = null;
}
