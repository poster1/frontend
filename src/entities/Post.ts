import type User from "@/entities/User";

export default class Post {
  public id?: number | null = null;
  public content?: string | null = null;
  public author?: User | null = null;
  public comments?: Comment[] | null = null;
}
