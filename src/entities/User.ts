export default class User {
  public id?: number | null = null;
  public email?: string | null = null;
  public password?: string | null = null;
  public firstName?: string | null = null;
  public lastName?: string | null = null;
  public roles?: string[] | null = null;
  public token?: string | null = null;
}
