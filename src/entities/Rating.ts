import type User from "@/entities/User";

export default class Rating {
  public id?: number | null = null;
  public value?: number | null = null;
  public comment?: Comment | null = null;
  public author?: User | null = null;
}
