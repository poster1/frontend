import { describe, expect, it } from "vitest";

import { mount } from "@vue/test-utils";
import CommentComponent from "@/views/components/CommentComponent.vue";

describe("CommentComponent", () => {
  it("renders properly", () => {
    const wrapper = mount(CommentComponent, { props: { msg: "Hello Vitest" } });
    expect(wrapper.text()).toContain("Hello Vitest");
  });
});
