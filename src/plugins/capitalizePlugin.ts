import type { App } from "vue";

export default {
  install: (app: App) => {
    app.config.globalProperties.$capitalize = (key: string) => {
      return key.slice(0, 1).toUpperCase() + key.slice(1);
    };
  },
};
