import useAuthStore from "@/stores/useAuthStore";
import type Keyable from "@/interfaces/Keyable";
import router from "@/router";
import useNotificationStore from "@/stores/useNotificationStore";

export default class Request {
  static BASE_URL = import.meta.env.VITE_API_URL;

  static async request<T>(url: string, init: RequestInit): Promise<T | null> {
    const authStore = useAuthStore();
    const notificationStore = useNotificationStore();

    if (authStore.token) {
      init.headers = {
        ...init.headers,
        Authorization: `Bearer ${authStore.token}`,
      };
    }

    init.headers = {
      ...init.headers,
      "Content-Type": "application/json",
    };

    try {
      const response = await fetch(`${this.BASE_URL}${url}`, init);
      if (response.status === 401) {
        if (!["/login", "/register"].includes(router.currentRoute.value.path)) {
          router.push("/login").then();
        }
      }

      const json = await response.json();

      if (response.ok) {
        return json.data as Promise<T>;
      }

      if (json.errors) {
        for (const error of json.errors) {
          notificationStore.error(
            // @ts-ignore
            Object.values(error)[0],
            Object.keys(error)[0]
          );
        }
      }

      if (json.message) {
        notificationStore.error(json.message);
      }

    } catch (e: Error | any) {
      notificationStore.error(e.message);
    }

    return null;
  }

  static async get<T>(url: string, init = {}): Promise<T | null> {
    init = {
      ...init,
      method: "GET",
    };

    return this.request<T>(url, init);
  }

  static async post<T>(
    url: string,
    body: Keyable = {},
    init = {}
  ): Promise<T | null> {
    init = {
      ...init,
      method: "POST",
      body: JSON.stringify(body),
    };

    return this.request<T>(url, init);
  }

  static async put<T>(url: string, body: any, init = {}): Promise<T | null> {
    init = {
      ...init,
      method: "PUT",
      body: JSON.stringify(body),
    };

    return this.request<T>(url, init);
  }

  static async delete<T>(url: string, body: any, init = {}): Promise<T | null> {
    init = {
      ...init,
      method: "DELETE",
    };

    return this.request<T>(url, init);
  }
}
