import { createRouter, createWebHistory } from "vue-router";
import useAuthStore from "@/stores/useAuthStore";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("@/views/pages/HomePage.vue"),
    },
    {
      path: "/post/:id",
      name: "post",
      props: true,
      component: () => import("@/views/pages/PostPage.vue"),
    },
    {
      path: "/login",
      name: "login",
      component: () => import("@/views/pages/LoginPage.vue"),
    },
    {
      path: "/register",
      name: "register",
      component: () => import("@/views/pages/RegisterPage.vue"),
    },
  ],
});

router.beforeEach(async (to) => {
  const authStore = useAuthStore();

  if (!["/login", "/register"].includes(to.path) && !authStore.isLoggedIn) {
    return "/login";
  }
});

export default router;
