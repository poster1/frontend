import { ref } from "vue";
import { defineStore } from "pinia";

const useAppStore = defineStore(
  "app",
  () => {
    const baseUrl = import.meta.env.VITE_BASE_URL;
    const search = ref("");

    return {
      baseUrl,
      search,
    };
  },
  {
    persist: true,
  }
);

export default useAppStore;
