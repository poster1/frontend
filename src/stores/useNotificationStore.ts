import { ref } from "vue";
import { defineStore } from "pinia";
import type Notification from "@/interfaces/Notification";

const useNotificationStore = defineStore("notification", () => {
  const notifications = ref<Notification[]>([]);

  const notify = (message: string, status: string, title?: string) => {
    notifications.value.push({
      uuid: Math.random().toString(36).substr(2, 9),
      message,
      title,
      status: "error",
    });
  };

  const error = (message: string, title?: string) => {
    notify(message, "error", title);
  };

  const success = (message: string, title?: string) => {
    notify(message, "success", title);
  };

  const info = (message: string, title?: string) => {
    notify(message, "info", title);
  };

  const warning = (message: string, title?: string) => {
    notify(message, "warning", title);
  };

  const remove = (id: string) => {
    notifications.value = notifications.value.filter((el) => el.uuid !== id);
  };

  const clear = () => {
    notifications.value = [];
  };

  return {
    notifications,
    notify,
    error,
    success,
    info,
    warning,
    remove,
    clear,
  };
});

export default useNotificationStore;
