import { computed, ref } from "vue";
import { defineStore } from "pinia";
import type Post from "@/entities/Post";
import Request from "@/services/api/Request";

const usePostStore = defineStore(
  "post",
  () => {
    const loading = ref(false);
    const initialized = ref(false);
    const posts = ref<Post[]>([]);

    const fetchPosts = async () => {
      loading.value = true;
      const data = await Request.get<Post[]>("/post");
      if (data) {
        posts.value = data;
        initialized.value = true;
      }
      loading.value = false;
    };

    const fetchPost = async (id: number): Promise<Post | null> => {
      const data = await Request.get<Post>("/post/" + id);
      if (data) {
        posts.value = posts.value.filter((post) => post.id !== data.id);
        posts.value.push(data);
      }

      return data;
    };

    const add = async (content: string) => {
      const data = await Request.post<Post>("/post", {
        content,
      });

      if (data) {
        posts.value.push(data);
      }
    };

    return {
      loading,
      initialized,
      posts,
      fetchPosts,
      fetchPost,
      add,
    };
  },
  {
    persist: false,
  }
);

export default usePostStore;
