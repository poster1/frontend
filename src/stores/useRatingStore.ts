import { ref } from "vue";
import { defineStore } from "pinia";
import type Rating from "@/entities/Rating";
import Request from "@/services/api/Request";

const useRatingStore = defineStore(
  "rating",
  () => {
    const loading = ref(false);
    const initialized = ref(false);
    const ratings = ref<Rating[]>([]);

    const fetchRatings = async () => {
      loading.value = true;
      const data = await Request.get<Rating[]>("/rating");
      if(data){
        ratings.value = data;
        initialized.value = true;
      }
      loading.value = false;
    };

    return {
      loading,
      initialized,
      ratings,
      fetchRatings,
    };
  },
  {
    persist: false,
  }
);

export default useRatingStore;
