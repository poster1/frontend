import { ref } from "vue";
import { defineStore } from "pinia";
import type Comment from "@/entities/Comment";
import Request from "@/services/api/Request";

const useCommentStore = defineStore(
  "comment",
  () => {
    const loading = ref(false);
    const initialized = ref(false);
    const comments = ref<Comment[]>([]);

    const fetchComments = async () => {
      loading.value = true;
      const data = await Request.get<Comment[]>("/comment");
      if(data){
        comments.value = data;
        initialized.value = true;
      }
      loading.value = false;
    };

    const add = async (postId: number, content: string, parentId = 0) => {
      const data = await Request.post<Comment>("/comment", {
        postId,
        content,
        parentId,
      });

      if (data) {
        comments.value.push(data);
      }
    };

    return {
      loading,
      initialized,
      comments,
      fetchComments,
      add,
    };
  },
  {
    persist: false,
  }
);

export default useCommentStore;
