import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/entities/User";
import router from "@/router";
import Request from "@/services/api/Request";

const useAuthStore = defineStore(
  "auth",
  () => {
    const user = ref<User | null>(null);
    const token = ref<string | null>(null);

    const isLoggedIn = computed(() => !!token.value);

    const login = async (email: string, password: string) => {
      const data = await Request.post<{
        user: User | null;
        token: string | null;
      }>("/user/login", {
        email,
        password,
      });

      if (data) {
        user.value = data.user;
        token.value = data.token;
        router.push("/").then();
      }
    };

    const register = async (payload: User) => {
      const data = await Request.post<{
        user: User | null;
        token: string | null;
      }>("/user/login", payload);

      if (data) {
        user.value = data.user;
        token.value = data.token;
        router.push("/").then();
      }
    };

    const logout = () => {
      user.value = null;
      token.value = null;
    };

    return { login, register, logout, user, token, isLoggedIn };
  },
  {
    persist: true,
  }
);

export default useAuthStore;
