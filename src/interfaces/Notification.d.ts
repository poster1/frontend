export default interface Notification {
  uuid?: string;
  title?: string;
  message: string;
  status: "success" | "error" | "info" | "warning";
}
