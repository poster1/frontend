const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx,vue}"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: "#42B883",
          50: "#C7EBDB",
          100: "#B8E5D1",
          200: "#9ADBBE",
          300: "#7CD0AA",
          400: "#5EC597",
          500: "#42B883",
          600: "#338F66",
          700: "#246548",
          800: "#163C2B",
          900: "#07130D",
        },
        secondary: {
          DEFAULT: "#35495E",
          50: "#8CA5BE",
          100: "#7F9AB7",
          200: "#6586A8",
          300: "#527292",
          400: "#445D78",
          500: "#35495E",
          600: "#212D3A",
          700: "#0D1116",
          800: "#000000",
          900: "#000000",
        },
      },
      fontFamily: {
        poppins: ["Poppins"],
        nunito: ["Nunito"],
        inter: ["Inter var", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [],
};
